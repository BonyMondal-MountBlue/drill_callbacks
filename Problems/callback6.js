/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/
const { GetBoardsInfousingID } = require("./callback1");

const { UsingboardIDgetListsdata } = require("./callback2");

const { CardsdetailsUsingListID } = require("./callback3");

module.exports = {
  callback6: function (boards, cards, lists) {
    setTimeout(() => {
      if (
        arguments.length < 3 ||
        typeof boards !== "object" ||
        typeof cards !== "object" ||
        typeof lists !== "object"
      ) {
        let err = new Error("Invalid Data");
        console.log(err);
      } else {
        boards.filter((tempBoards) => {
          if (tempBoards.name === "Thanos") {
            let boards1 = [];
            boards1.push(tempBoards);
            GetBoardsInfousingID(tempBoards.id, boards1, (err, data) => {
              if (err) {
                console.log(err);
              } else {
                let tempdata = [];
                tempdata.push(data);
                tempdata.filter((tempdataID) => {
                  UsingboardIDgetListsdata(
                    tempdataID.id,
                    lists,
                    (err, data) => {
                      if (err) {
                        console.log(err);
                      } else {
                        data.forEach((tempData) => {
                          CardsdetailsUsingListID(
                            tempData.id,
                            cards,
                            (err, data) => {
                              if (err) {
                                console.log(err);
                              } else {
                                data.forEach((tempdata) => {
                                  console.log(tempdata);
                                });
                              }
                            }
                          );
                        });
                      }
                    }
                  );
                });
              }
            });
          }
        });
      }
    }, 2 * 1000);
  }
};
