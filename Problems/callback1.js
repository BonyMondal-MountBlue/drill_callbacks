/* 
Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/
module.exports = {
  GetBoardsInfousingID: function (boardID, boards, callback) {
    setTimeout(() => {
      try {
        if (
          arguments.length === 3 &&
          typeof boardID === "string" &&
          typeof boards === "object"
        ) {
          let data = boards.find(
            (boardIDdetails) => boardIDdetails.id === boardID
          );
          if (data) {
            callback(null, data);
          } else {
            let err = new Error("Invalid ID");
            callback(err);
          }
        }
      } catch (err) {
        err = new Error("Invalid Data");
        callback(err);
      }

      // try {
      //   let data = boards.find((boardIDdetails) => boardIDdetails.id === boardID);
      //   if (data) {
      //     callback(null, data);
      //   } else {
      //     let err = new Error("No data found");
      //     callback(err);
      //   }
      // } catch (err) {
      //   err = new Error("Invalid Data");
      //   callback(err);
      // }
    }, 2 * 1000);
  }
};
