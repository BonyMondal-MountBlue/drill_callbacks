/* 
Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/
module.exports = {
  UsingboardIDgetListsdata: function (boardID, lists, callback) {
    setTimeout(() => {
      /**
       * boardID will take random ID if the ID matches with lists ID it will take all the data.
       */
      try {
        if (
          arguments.length === 3 &&
          typeof boardID === "string" &&
          typeof lists === "object"
        ) {
          let data = lists[boardID];

          if (data) {
            callback(null, data);
          } else {
            callback(new Error("Invalid ID"));
          }
        }
      } catch (err) {
        err = new Error("Invalid Data");
        callback(err);
      }

      // try {
      //   let data = lists[boardID];

      //   if (data) {
      //     callback(null, data);
      //   } else {
      //     let err = new Error("No data found");
      //     callback(err);
      //   }
      // } catch(err) {
      //   err = new Error("No data found");
      //   callback(err);
      // }
    }, 2 * 1000);
  }
};
