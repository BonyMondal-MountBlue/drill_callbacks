const path = require("path");

const { UsingboardIDgetListsdata } = require("../callback2");

const lists = require(path.join(__dirname, "../../Data/lists.json"));

const boards = require(path.join(__dirname, "../../Data/boards.json"));

UsingboardIDgetListsdata(
  "mcu453ed",
  lists,
  (error, data) => {
    if (error) {
      console.log(error);
    } else {
      console.log(data);
    }
  }
);

UsingboardIDgetListsdata(
  "mcu453ed",
  null,
  (error, data) => {
    if (error) {
      console.log(error);
    } else {
      console.log(data);
    }
  }
);

UsingboardIDgetListsdata(
  "abc",
  lists,
  (error, data) => {
    if (error) {
      console.log(error);
    } else {
      console.log(data);
    }
  }
);

UsingboardIDgetListsdata(
  "mcu453eddd",[],
  (error, data) => {
    if (error) {
      console.log(error);
    } else {
      console.log(data);
    }
  }
);


