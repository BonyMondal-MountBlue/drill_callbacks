const path = require("path");

const boards = require(path.join(__dirname,"../../Data/boards.json")); 
/**
 * import data from boards
 */
const { GetBoardsInfousingID } = require("../callback1.js");
/**
 * Now calling the callback function if error it will throw error or else give required data.
 */
/**
 * [Math.floor(Math.random() * boards.length)] use to generate random keys,values in a particular object
 */
GetBoardsInfousingID("mcu453ed", boards, (error, data) => {
  if (error) {
    console.log(error);
  } else {
    console.log(data);
  }
});


GetBoardsInfousingID("mcu453ed", null, (error, data) => {
  if (error) {
    console.log(error);
  } else {
    console.log(data);
  }
});

GetBoardsInfousingID("mcu453eddd", boards, (error, data) => {
  if (error) {
    console.log(error);
  } else {
    console.log(data);
  }
});

GetBoardsInfousingID("mcu453eddd",[], (error, data) => {
  if (error) {
    console.log(error);
  } else {
    console.log(data);
  }
});

