const path = require("path");
const boards = require(path.join(__dirname, "../../Data/boards.json"));
const lists = require(path.join(__dirname, "../../Data/lists.json"));
const cards = require(path.join(__dirname, "../../Data/cards.json"));
const {callback4} = require("../callback4");

// callback4(boards, cards, lists, (error, data) => {
//   if (error) {
//     console.log(error);
//   } else {
//     console.log(data);
//   }
// });

callback4(boards,cards,lists);

