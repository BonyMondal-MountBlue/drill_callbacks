const path = require("path");

const { CardsdetailsUsingListID } = require("../callback3");

const lists = require(path.join(__dirname, "../../Data/lists.json"));

const cards = require(path.join(__dirname, "../../Data/cards.json"));

CardsdetailsUsingListID("qwsa221", cards, (error, data) => {
  if (error) {
    console.log(error);
  } else {
    console.log(data);
  }
});

CardsdetailsUsingListID("qwsa2211", cards, (error, data) => {
  if (error) {
    console.log(error);
  } else {
    console.log(data);
  }
});

CardsdetailsUsingListID(123, cards, (error, data) => {
  if (error) {
    console.log(error);
  } else {
    console.log(data);
  }
});
