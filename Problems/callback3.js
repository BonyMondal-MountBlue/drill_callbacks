/* 
Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

module.exports = {
  CardsdetailsUsingListID: function (listID, cards, callback) {
    setTimeout(() => {
      try {
        if (
          arguments.length === 3 &&
          typeof listID === "string" &&
          typeof cards === "object"
        ) {
          let data = cards[listID];

          if (data) {
            callback(null, data);
          } else {
            callback(new Error("Invalid ID"));
          }
        } else {
          callback(new Error("Invalid data"));
        }
      } catch (err) {
        err = new Error("No data found!");
        callback(err);
      }
      // try {
      //   let data = cards[listID];
      //   if (data) {
      //     callback(null, data);
      //   } else {
      //     let err = new Error("No data found!");
      //     callback(err);
      //   }
      // } catch (err) {
      //   err = new Error("No data found!");
      //   callback(err)
      // }
    }, 2 * 1000);
  }
};
