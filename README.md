# Drill_Callbacks

## How the given data is associated:
```
1. Boards have ids and names

2. Lists belong to boards

3. Cards belong to lists
```
Create a function for each problem in a file called callback1.js, callback2.js, callback3.js and so on in the root of the project.

Ensure that the functions in each file is exported and tested in its own file called testCallback1.js, testCallback2.js, testCallback3.js and so on in a folder called test.

So your directory structure will look like this
```
├── callback1.js
├── callback2.js
├── callback3.js
├── callback4.js
├── callback5.js
├── callback6.js
└── test
      ├── testCallback1.js
      ├── testCallback2.js
      ├── testCallback3.js
      ├── testCallback4.js
      ├── testCallback5.js
      └── testCallback6.js
```
## If you have VS code installed on your system:
 
 1. Create above structures of directories and files using CLI (e.g. mkdir, touch)
 
 2. Make sure you run
     ```git init```, ```npm init```,    ```npm install --package-lock```   for initiated a git repo and node packagees.
## Must use in problems:
```
Each function that you write must take at least 2s to execute using the setTimeout function like so:

	function() {
		setTimeout(() => {
			// Your code here
		}, 2 * 1000);
	}
```
## Problems:
```
/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/
```
```
/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/
```
## If you want to clone my repository:
use:
```
git clone -b <my-branch> <my-git url(ssh)>
```
## Run the functions using scripts:
```
npm run start1
npm run start2
npm run start3
npm run start4
npm run start5
npm run start6
```


